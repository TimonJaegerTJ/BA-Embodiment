using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{
    void Start()
    {
       
    }
    public static void Load(string scenename)
    {
        SceneManager.LoadScene(scenename, LoadSceneMode.Additive);
    }

    
}
