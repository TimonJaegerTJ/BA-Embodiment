using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class TextRotation : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject mainCamera;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float speed = 1.0f;
    [SerializeField] private bool rotateAroundZ;
    [SerializeField] private bool rotationTowards;
    [SerializeField] private bool rotate3Axis;
    private Quaternion _currentRotation;
    private void Update()
    {
        if (rotate3Axis)
        {
            RotateFully();
        }
        if (rotateAroundZ)
        {
            RotateTowardsSimpler();
        }

        if (rotationTowards)
        {
            RotateTowardsPlayer();
        }
    }
    
    void RotateFully() => transform.rotation = Quaternion.Lerp(transform.rotation,mainCamera.transform.rotation, rotationSpeed * Time.deltaTime);
    
    void RotateTowardsSimpler()
    {
        _currentRotation = Quaternion.Lerp(
            transform.rotation,
            player.transform.rotation,
            rotationSpeed * Time.deltaTime);
        _currentRotation.x = 0; 
        _currentRotation.z = 0; 
        transform.rotation = _currentRotation;
        
    }

    void RotateTowardsPlayer()
    {
        // Determine which direction to rotate towards
        Vector3 targetDirection = player.transform.position - transform.position;

        // The step size is equal to speed times frame time.
        float singleStep = speed * Time.deltaTime;

        // Rotate the forward vector towards the target direction by one step
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);

        // Calculate a rotation a step closer to the target and applies rotation to this object
        transform.rotation = Quaternion.LookRotation(newDirection);
        
        
    } 
}
