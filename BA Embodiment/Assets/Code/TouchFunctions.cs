using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TouchFunctions : MonoBehaviour
{
    
    public void QuitExperiment() => Application.Quit();
    
    //public void LoadEmbodimentScene() => SceneManager.LoadScene("Embodiment", LoadSceneMode.Additive);
    public void LoadEnvironmentScene() => SceneManager.LoadScene("Environment", LoadSceneMode.Additive);

    public void StartChangeEffect()
    {
        
    }
}
