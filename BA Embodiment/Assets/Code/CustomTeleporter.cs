using System;
using System.Collections;
using System.Collections.Generic;
using Oculus.Platform.Samples.VrHoops;
using UnityEngine;

public class CustomTeleporter : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject head;
    [SerializeField] private GameObject hand;
    [SerializeField] private GameObject hintParticleThumb;
    [SerializeField] private GameObject hintParticleMiddleF;
    [SerializeField] private float teleportFactor;
    

    private void Update()
    {
        
    }

    private void OnTriggerExit(Collider finger)
    {
        if (finger.CompareTag("MiddleFingerTip"))
        {
            Debug.Log("Teleported?");
            MovePlayer();
            var heading = hand.transform.position - head.transform.position;
            heading.y = 0;  // This is the overground heading.
            hintParticleThumb.SetActive(false);
            hintParticleMiddleF.SetActive(false);
            Debug.DrawLine (hand.transform.position, head.transform.position + heading * 10, Color.red, Mathf.Infinity);
            //player.transform.position += Vector3.heading;
        }
    }
    private void OnTriggerEnter(Collider finger)
    {
        if (finger.CompareTag("MiddleFingerTip"))
        {
            
            hintParticleThumb.SetActive(true);
            hintParticleMiddleF.SetActive(true);
        }
    }

    void MovePlayer()
    {
        
        Vector3 heading = Camera.main.transform.forward;
        Debug.Log("heading before? "+ heading);
        heading.y = 0;
        Debug.Log("heading after? " + heading);
        player.transform.position += heading * teleportFactor;
    
    }
}

