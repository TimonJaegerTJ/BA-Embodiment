using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider))]
internal class ButtonTrigger : Button
{
    private void OnTriggerEnter(Collider other) 
    {
        if (other.CompareTag("IndexFingerTip"))
        {
            StartCoroutine(ButtonReaction());
            
        }
    }

    private IEnumerator ButtonReaction()
    {
        
        yield return new WaitForSeconds(0.3f);
        ExecuteEvents.Execute(gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.submitHandler);
    }
}

