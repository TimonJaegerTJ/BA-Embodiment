using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;


public class ActionPointManager : MonoBehaviour
{
    
    [SerializeField] private float pivotTurnOnDistance;
    [SerializeField] private GameObject player;
    //[SerializeField] private TextMeshPro uIText;
    [SerializeField] private GameObject board;
    [SerializeField] private GameObject indicator;
    [SerializeField] private GameObject cylinder;
    [SerializeField] private Vector3 sizeOne;
    [SerializeField] private Vector3 sizeTwo;
    [SerializeField] private Vector3 sizeIndicator;
    //[SerializeField] private string explanation;
    
    void Update()
    {
        var dist = Vector3.Distance(player.transform.position, transform.position);
        
        if (dist > pivotTurnOnDistance)
        {
            
            indicator.transform.localScale = Vector3.Lerp(board.transform.localScale, sizeTwo, 2 * Time.deltaTime);
            board.transform.localScale = Vector3.Lerp(board.transform.localScale, sizeTwo, 2 * Time.deltaTime);
            cylinder.transform.localScale = Vector3.Lerp(board.transform.localScale, sizeOne, 2 * Time.deltaTime);
            //uIText.text = gameObject.name;
            
        }
        if (dist < pivotTurnOnDistance)
        {
            indicator.transform.localScale = Vector3.Lerp(board.transform.localScale, sizeIndicator, 2 * Time.deltaTime);
            board.transform.localScale = Vector3.Lerp(board.transform.localScale, sizeOne, 2 * Time.deltaTime);
            cylinder.transform.localScale = Vector3.Lerp(board.transform.localScale, sizeTwo, 2 * Time.deltaTime);
            
            //uIText.text = gameObject.name + ": " + explanation;
        }
    }
}
