using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
//using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class TabletSetter : MonoBehaviour
{
    
    
    private void Start()
    {
        foreach (Transform child in gameObject.transform)
        {
            if (child.CompareTag("SwitchedOn"))
                
            {
                child.gameObject.SetActive(true);
                
            }
            
            else if (child.CompareTag("SwitchedOff"))
            {
                child.gameObject.SetActive(false);
                
            }
                
        }
        
    }
}
