using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.XR.Interaction.Toolkit;
using Random = UnityEngine.Random;

public class TabletFloating : MonoBehaviour
{
    private MeshRenderer _meshRendererBubble;
    
    private Vector3 _randomRotation;
    private Vector3 _positionLeftHand;
    private Vector3 _positionRightHand;

    private Rigidbody _rb;
    
    [SerializeField] private GameObject bubble;
    [SerializeField] private GameObject head;
    [SerializeField] private GameObject promptText;
    [SerializeField] private GameObject attachmentPointUpdated;
    [SerializeField] private GameObject attachmentPointRight;
    [SerializeField] private GameObject attachmentPointLeft;
    [SerializeField] private GameObject tabletProxy;
    [SerializeField] private float smoothFactor = 500f;
    private Vector3 _velocity = Vector3.zero;
    
    private void Awake()
    {
        _meshRendererBubble = bubble.GetComponent<MeshRenderer>();
        _rb = GetComponent<Rigidbody>();
        _randomRotation = new Vector3(Random.Range(0.05f,0.1f), Random.Range(0.05f,0.1f), Random.Range(0.05f,0.1f));
        
    }

    private void OnTriggerEnter(Collider handApproaching)
    {
        if (handApproaching.CompareTag("LeftHand"))
        {
            attachmentPointUpdated.transform.position = attachmentPointLeft.transform.position;
            attachmentPointUpdated.transform.rotation = attachmentPointLeft.transform.rotation;
            //TurnOffRightHandsTouch();
        }
        if (handApproaching.CompareTag("RightHand"))
        {
            attachmentPointUpdated.transform.position = attachmentPointRight.transform.position;
            attachmentPointUpdated.transform.rotation = attachmentPointRight.transform.rotation;
            
        }
    }
    

    private void Update()
    {
        Rotate();
        FollowPlayer();
    }

    private void Rotate() => transform.Rotate(_randomRotation);
    private void FollowPlayer()
    {
        var dist = Vector3.Distance(head.transform.position, transform.position);
        
        if (dist > 0.8f)
        {
            transform.position = Vector3.SmoothDamp(
                transform.position,
                tabletProxy.transform.position,
                ref _velocity,
                smoothFactor * Time.deltaTime);
        }
        if (dist < 0.4f)
        {
            transform.position = Vector3.SmoothDamp(
                transform.position,
                tabletProxy.transform.position,
                ref _velocity,
                smoothFactor * Time.deltaTime);
        }
        
    }

    public void ToggleBubbleRendererOff() => _meshRendererBubble.enabled = false;
    public void ToggleBubbleRendererOn() => _meshRendererBubble.enabled = true;
    public void TogglePromptTextOff() => promptText.SetActive(false);
    public void TogglePromptTextOn() => promptText.SetActive(true);

    //void TurnOffRightHandsTouch()
    //{
    //    var fingerToBeTurnedOff = GameObject.Find("hands:b_r_middle_ignore");
    //    fingerToBeTurnedOff.GetComponent<Collider>().enabled = false;
    //}

}
