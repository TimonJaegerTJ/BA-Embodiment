using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class UISelecter : MonoBehaviour
{
    [SerializeField] private GameObject startMenu;
    [SerializeField] private GameObject characterMenu;
    [SerializeField] private GameObject continoueButton;
    private string _buttonName;
    private void Start()
    {
        _buttonName = gameObject.name;
        
    }

    IEnumerator ButtonReaction()
    {
        
        yield return new WaitForSeconds(0.5f);
    }
    void OnTriggerEnter(Collider finger)
    {
        if (finger.CompareTag("IndexFingerTip") && name == "ButtonStart")
        {
            
            Color col = new Color(0, 255, 0);
            GetComponent<Renderer>().material.color = col;

            Loader.Load("Embodiment");
            
            // wait to make colour change be seen
            StartCoroutine(ButtonReaction());
            
            
            
        }
        else if(finger.CompareTag("IndexFingerTip") && name == "ButtonExit")
        {
            //change colour when pressed
            Color col = new Color(255, 000, 0);
            GetComponent<Renderer>().material.color = col;
            // wait to make colour change be seen
            StartCoroutine(ButtonReaction());
            
            //quit
            //Application.Quit();
        }
        
        else if(finger.CompareTag("Fingertip") &&
                name == "c01" ||
                name == "c02" ||
                name == "c03" ||
                name == "c04" ||
                name == "c05")
        {
            //change colour when pressed
            Color col = new Color(0, 255, 0);
            GetComponent<Renderer>().material.color = col;

            //quit
            //Application.Quit();
        }
    }

    void ChangeColorToBlack()
    {
        Color col = new Color(0, 0, 0);
        GetComponent<Renderer>().material.color = col;
    }
}
